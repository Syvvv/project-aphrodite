﻿namespace DungeonsAndDragons
{
    partial class DnDForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.contractsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recurringGoodsContractToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recurringServiceContractToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.singleGoodsContractToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.singleServiceContractToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.advanceDayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Container = new System.Windows.Forms.GroupBox();
            this.DateDisplay = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contractsToolStripMenuItem,
            this.toolsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(2313, 40);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // contractsToolStripMenuItem
            // 
            this.contractsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listToolStripMenuItem,
            this.createToolStripMenuItem});
            this.contractsToolStripMenuItem.Name = "contractsToolStripMenuItem";
            this.contractsToolStripMenuItem.Size = new System.Drawing.Size(127, 36);
            this.contractsToolStripMenuItem.Text = "Contracts";
            // 
            // listToolStripMenuItem
            // 
            this.listToolStripMenuItem.Name = "listToolStripMenuItem";
            this.listToolStripMenuItem.Size = new System.Drawing.Size(183, 38);
            this.listToolStripMenuItem.Text = "List";
            this.listToolStripMenuItem.Click += new System.EventHandler(this.ListToolStripMenuItem_Click);
            // 
            // createToolStripMenuItem
            // 
            this.createToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.recurringGoodsContractToolStripMenuItem,
            this.recurringServiceContractToolStripMenuItem,
            this.singleGoodsContractToolStripMenuItem,
            this.singleServiceContractToolStripMenuItem});
            this.createToolStripMenuItem.Name = "createToolStripMenuItem";
            this.createToolStripMenuItem.Size = new System.Drawing.Size(183, 38);
            this.createToolStripMenuItem.Text = "Create";
            // 
            // recurringGoodsContractToolStripMenuItem
            // 
            this.recurringGoodsContractToolStripMenuItem.Name = "recurringGoodsContractToolStripMenuItem";
            this.recurringGoodsContractToolStripMenuItem.Size = new System.Drawing.Size(395, 38);
            this.recurringGoodsContractToolStripMenuItem.Text = "Recurring Goods Contract";
            this.recurringGoodsContractToolStripMenuItem.Click += new System.EventHandler(this.RecurringGoodsContractToolStripMenuItem_Click);
            // 
            // recurringServiceContractToolStripMenuItem
            // 
            this.recurringServiceContractToolStripMenuItem.Name = "recurringServiceContractToolStripMenuItem";
            this.recurringServiceContractToolStripMenuItem.Size = new System.Drawing.Size(395, 38);
            this.recurringServiceContractToolStripMenuItem.Text = "Recurring Service Contract";
            this.recurringServiceContractToolStripMenuItem.Click += new System.EventHandler(this.RecurringServiceContractToolStripMenuItem_Click);
            // 
            // singleGoodsContractToolStripMenuItem
            // 
            this.singleGoodsContractToolStripMenuItem.Name = "singleGoodsContractToolStripMenuItem";
            this.singleGoodsContractToolStripMenuItem.Size = new System.Drawing.Size(395, 38);
            this.singleGoodsContractToolStripMenuItem.Text = "Single Goods Contract";
            this.singleGoodsContractToolStripMenuItem.Click += new System.EventHandler(this.SingleGoodsContractToolStripMenuItem_Click);
            // 
            // singleServiceContractToolStripMenuItem
            // 
            this.singleServiceContractToolStripMenuItem.Name = "singleServiceContractToolStripMenuItem";
            this.singleServiceContractToolStripMenuItem.Size = new System.Drawing.Size(395, 38);
            this.singleServiceContractToolStripMenuItem.Text = "Single Service Contract";
            this.singleServiceContractToolStripMenuItem.Click += new System.EventHandler(this.SingleServiceContractToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.advanceDayToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(82, 36);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // advanceDayToolStripMenuItem
            // 
            this.advanceDayToolStripMenuItem.Name = "advanceDayToolStripMenuItem";
            this.advanceDayToolStripMenuItem.Size = new System.Drawing.Size(253, 38);
            this.advanceDayToolStripMenuItem.Text = "Advance Day";
            this.advanceDayToolStripMenuItem.Click += new System.EventHandler(this.AdvanceDayToolStripMenuItem_Click);
            // 
            // Container
            // 
            this.Container.Location = new System.Drawing.Point(13, 56);
            this.Container.Name = "Container";
            this.Container.Size = new System.Drawing.Size(2261, 1197);
            this.Container.TabIndex = 1;
            this.Container.TabStop = false;
            // 
            // DateDisplay
            // 
            this.DateDisplay.AutoSize = true;
            this.DateDisplay.Location = new System.Drawing.Point(2095, 0);
            this.DateDisplay.Name = "DateDisplay";
            this.DateDisplay.Size = new System.Drawing.Size(74, 25);
            this.DateDisplay.TabIndex = 2;
            this.DateDisplay.Text = "Day: 0";
            // 
            // DnDForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2313, 1299);
            this.Controls.Add(this.DateDisplay);
            this.Controls.Add(this.Container);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(2339, 1370);
            this.Name = "DnDForm";
            this.Text = "Dungeons And Dragons tool";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DnDForm_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem contractsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createToolStripMenuItem;
        private System.Windows.Forms.GroupBox Container;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem advanceDayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recurringGoodsContractToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recurringServiceContractToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem singleGoodsContractToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem singleServiceContractToolStripMenuItem;
        private System.Windows.Forms.Label DateDisplay;
    }
}

