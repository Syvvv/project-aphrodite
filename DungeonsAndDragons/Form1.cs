﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DungeonsAndDragons
{
    public partial class DnDForm : Form
    {
        Data data;

        public DnDForm()
        {
            InitializeComponent();
            if(FileLoader.FileExists("save.dat"))
            {
                data = FileLoader.Read<Data>("save.dat");
            }
            else
            {
                data = new Data();
            }
            DateDisplay.Text = "Day: " + data.CurrentDate;
        }

        private void DnDForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            FileLoader.Write(data, "save.dat");
        }

        private void AdvanceDayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(this, data.NextDay(), "Day advanced!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            DateDisplay.Text = "Day: " + data.CurrentDate;
        }

        private void Clear()
        {
            Container.Controls.Clear();
        }

        private void RecurringGoodsContractToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clear();

            Label contractorlbl = new Label { Top = 0, Left = 0, Text = "Contractor" , Width = 100};
            TextBox contractor = new TextBox { Top = 0, Left = 100};
            Label contracteelbl = new Label { Top = 25, Left = 0, Text = "Contractee", Width = 100 };
            TextBox contractee = new TextBox { Top = 25, Left = 100 };
            Label goodslbl = new Label { Top = 50, Left = 0, Text = "Goods", Width = 100 };
            TextBox goods = new TextBox { Top = 50, Left = 100, Width = 400};
            Label startDaylbl = new Label { Top = 75, Left = 0, Text = "Start Day", Width = 100 };
            TextBox startDay = new TextBox { Top = 75, Left = 100, Text = "0"};
            Label endDaylbl = new Label { Top = 100, Left = 0, Text = "End Day", Width = 100 };
            TextBox endDay = new TextBox { Top = 100, Left = 100, Text = "0" };
            Label dayslbl = new Label { Top = 125, Left = 0, Text = "Days between deliveries", Width = 200 };
            TextBox days = new TextBox { Top = 125, Left = 200, Text = "0" };
            Label pricelbl = new Label { Top = 150, Left = 0, Text = "Price", Width = 100 };
            TextBox price = new TextBox { Top = 150, Left = 100, Text = "0" };
            Label UpFrontlbl = new Label { Top = 175, Left = 0, Text = "Up Front Cost", Width = 100 };
            TextBox UpFront = new TextBox { Top = 175, Left = 100, Text = "0" };
            Label paymentlbl = new Label { Top = 200, Left = 0, Text = "Deposit?", Width = 100 };
            CheckBox payment = new CheckBox { Top = 200, Left = 100 };
            Label discountlbl = new Label { Top = 225, Left = 0, Text = "Discount", Width = 100 };
            TextBox discount = new TextBox { Top = 225, Left = 100, Text = "0" };
            Label discounttimelbl = new Label { Top = 250, Left = 0, Text = "Discount Time", Width = 100 };
            TextBox discounttime = new TextBox { Top = 250, Left = 100, Text = "0" };
            Label originlbl = new Label { Top = 275, Left = 0, Text = "Origin Place", Width = 100 };
            TextBox origin = new TextBox { Top = 275, Left = 100 };
            Label ddestinationlbl = new Label { Top = 300, Left = 0, Text = "Destination", Width = 100 };
            TextBox destination = new TextBox { Top = 300, Left = 100 };
            Label cancellationPeriodlbl = new Label { Top = 325, Left = 0, Text = "Cancellation period", Width = 100 };
            TextBox cancellationPeriod = new TextBox { Top = 325, Left = 100, Text = "0" };
            Button submit = new Button { Top = 350, Left = 50, Width = 100, Text = "Agree" };
            submit.Click+= (s,ea) =>{
                Contract contract = new Contract(ContractType.RECURRING_GOODS, int.Parse(startDay.Text), double.Parse(price.Text), contractor.Text, contractee.Text)
                {
                    CancellationPeriod = int.Parse(cancellationPeriod.Text),
                    DayInterval = int.Parse(days.Text),
                    DestinationPlace = destination.Text,
                    Discount = double.Parse(discount.Text),
                    DiscountTime_Remaining = int.Parse(discounttime.Text),
                    EndDate = int.Parse(endDay.Text),
                    Finished = false,
                    Goods = goods.Text,
                    OriginPlace = origin.Text,
                    UpFrontCost = double.Parse(UpFront.Text)
                };
                if (payment.Checked)
                {
                    contract.UpFrontType = UpfrontType.PAYMENT;
                }
                else
                {
                    contract.UpFrontType = UpfrontType.DEPOSIT;
                }

                data.Contracts.Add(contract);
                Clear();
            };

            Container.Controls.Add(contractorlbl);
            Container.Controls.Add(contractor);
            Container.Controls.Add(contracteelbl);
            Container.Controls.Add(contractee);
            Container.Controls.Add(goodslbl);
            Container.Controls.Add(goods);
            Container.Controls.Add(startDaylbl);
            Container.Controls.Add(startDay);
            Container.Controls.Add(endDaylbl);
            Container.Controls.Add(endDay);
            Container.Controls.Add(dayslbl);
            Container.Controls.Add(days);
            Container.Controls.Add(pricelbl);
            Container.Controls.Add(price);
            Container.Controls.Add(UpFrontlbl);
            Container.Controls.Add(UpFront);
            Container.Controls.Add(paymentlbl);
            Container.Controls.Add(payment);
            Container.Controls.Add(discountlbl);
            Container.Controls.Add(discount);
            Container.Controls.Add(discounttimelbl);
            Container.Controls.Add(discounttime);
            Container.Controls.Add(originlbl);
            Container.Controls.Add(origin);
            Container.Controls.Add(ddestinationlbl);
            Container.Controls.Add(destination);
            Container.Controls.Add(cancellationPeriodlbl);
            Container.Controls.Add(cancellationPeriod);
            Container.Controls.Add(submit);

        }

        private void RecurringServiceContractToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clear();

            Label contractorlbl = new Label { Top = 0, Left = 0, Text = "Contractor", Width = 100 };
            TextBox contractor = new TextBox { Top = 0, Left = 100 };
            Label contracteelbl = new Label { Top = 25, Left = 0, Text = "Contractee", Width = 100 };
            TextBox contractee = new TextBox { Top = 25, Left = 100 };
            Label servicelbl = new Label { Top = 50, Left = 0, Text = "Service", Width = 100 };
            TextBox service = new TextBox { Top = 50, Left = 100, Width = 400 };
            Label startDaylbl = new Label { Top = 75, Left = 0, Text = "Start Day", Width = 100 };
            TextBox startDay = new TextBox { Top = 75, Left = 100, Text = "0" };
            Label endDaylbl = new Label { Top = 100, Left = 0, Text = "End Day", Width = 100 };
            TextBox endDay = new TextBox { Top = 100, Left = 100, Text = "0" };
            Label dayslbl = new Label { Top = 125, Left = 0, Text = "Days between each service", Width = 200 };
            TextBox days = new TextBox { Top = 125, Left = 200, Text = "0" };
            Label pricelbl = new Label { Top = 150, Left = 0, Text = "Price", Width = 100 };
            TextBox price = new TextBox { Top = 150, Left = 100, Text = "0" };
            Label UpFrontlbl = new Label { Top = 175, Left = 0, Text = "Up Front Cost", Width = 100 };
            TextBox UpFront = new TextBox { Top = 175, Left = 100, Text = "0" };
            Label paymentlbl = new Label { Top = 200, Left = 0, Text = "Deposit?", Width = 100 };
            CheckBox payment = new CheckBox { Top = 200, Left = 100 };
            Label discountlbl = new Label { Top = 225, Left = 0, Text = "Discount", Width = 100 };
            TextBox discount = new TextBox { Top = 225, Left = 100, Text = "0" };
            Label discounttimelbl = new Label { Top = 250, Left = 0, Text = "Discount Time", Width = 100 };
            TextBox discounttime = new TextBox { Top = 250, Left = 100, Text = "0" };
            Label originlbl = new Label { Top = 275, Left = 0, Text = "Origin Place", Width = 100 };
            TextBox origin = new TextBox { Top = 275, Left = 100 };
            Label ddestinationlbl = new Label { Top = 300, Left = 0, Text = "Destination", Width = 100 };
            TextBox destination = new TextBox { Top = 300, Left = 100 };
            Label cancellationPeriodlbl = new Label { Top = 325, Left = 0, Text = "Cancellation period", Width = 100 };
            TextBox cancellationPeriod = new TextBox { Top = 325, Left = 100, Text = "0" };
            Button submit = new Button { Top = 350, Left = 50, Width = 100, Text = "Agree" };
            submit.Click += (s, ea) => {
                Contract contract = new Contract(ContractType.RECURRING_SERVICE, int.Parse(startDay.Text), double.Parse(price.Text), contractor.Text, contractee.Text)
                {
                    CancellationPeriod = int.Parse(cancellationPeriod.Text),
                    DayInterval = int.Parse(days.Text),
                    DestinationPlace = destination.Text,
                    Discount = double.Parse(discount.Text),
                    DiscountTime_Remaining = int.Parse(discounttime.Text),
                    EndDate = int.Parse(endDay.Text),
                    Finished = false,
                    Service = service.Text,
                    OriginPlace = origin.Text,
                    UpFrontCost = double.Parse(UpFront.Text)
                };
                if (payment.Checked)
                {
                    contract.UpFrontType = UpfrontType.PAYMENT;
                }
                else
                {
                    contract.UpFrontType = UpfrontType.DEPOSIT;
                }

                data.Contracts.Add(contract);
                Clear();
            };

            Container.Controls.Add(contractorlbl);
            Container.Controls.Add(contractor);
            Container.Controls.Add(contracteelbl);
            Container.Controls.Add(contractee);
            Container.Controls.Add(servicelbl);
            Container.Controls.Add(service);
            Container.Controls.Add(startDaylbl);
            Container.Controls.Add(startDay);
            Container.Controls.Add(endDaylbl);
            Container.Controls.Add(endDay);
            Container.Controls.Add(dayslbl);
            Container.Controls.Add(days);
            Container.Controls.Add(pricelbl);
            Container.Controls.Add(price);
            Container.Controls.Add(UpFrontlbl);
            Container.Controls.Add(UpFront);
            Container.Controls.Add(paymentlbl);
            Container.Controls.Add(payment);
            Container.Controls.Add(discountlbl);
            Container.Controls.Add(discount);
            Container.Controls.Add(discounttimelbl);
            Container.Controls.Add(discounttime);
            Container.Controls.Add(originlbl);
            Container.Controls.Add(origin);
            Container.Controls.Add(ddestinationlbl);
            Container.Controls.Add(destination);
            Container.Controls.Add(cancellationPeriodlbl);
            Container.Controls.Add(cancellationPeriod);
            Container.Controls.Add(submit);
        }

        private void SingleGoodsContractToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clear();

            Label contractorlbl = new Label { Top = 0, Left = 0, Text = "Contractor", Width = 100 };
            TextBox contractor = new TextBox { Top = 0, Left = 100 };
            Label contracteelbl = new Label { Top = 25, Left = 0, Text = "Contractee", Width = 100 };
            TextBox contractee = new TextBox { Top = 25, Left = 100 };
            Label goodslbl = new Label { Top = 50, Left = 0, Text = "Goods", Width = 100 };
            TextBox goods = new TextBox { Top = 50, Left = 100, Width = 400 };
            Label startDaylbl = new Label { Top = 75, Left = 0, Text = "Start Day", Width = 100 };
            TextBox startDay = new TextBox { Top = 75, Left = 100, Text = "0" };
            Label endDaylbl = new Label { Top = 100, Left = 0, Text = "End Day", Width = 100 };
            TextBox endDay = new TextBox { Top = 100, Left = 100, Text = "0" };
            Label pricelbl = new Label { Top = 125, Left = 0, Text = "Price", Width = 100 };
            TextBox price = new TextBox { Top = 125, Left = 100, Text = "0" };
            Label UpFrontlbl = new Label { Top = 150, Left = 0, Text = "Up Front Cost", Width = 100 };
            TextBox UpFront = new TextBox { Top = 150, Left = 100, Text = "0" };
            Label originlbl = new Label { Top = 175, Left = 0, Text = "Origin Place", Width = 100 };
            TextBox origin = new TextBox { Top = 175, Left = 100 };
            Label ddestinationlbl = new Label { Top = 200, Left = 0, Text = "Destination", Width = 100 };
            TextBox destination = new TextBox { Top = 200, Left = 100 };
            Button submit = new Button { Top = 225, Left = 50, Width = 100, Text = "Agree" };
            submit.Click += (s, ea) => {
                Contract contract = new Contract(ContractType.SINGLE_GOODS_DELIVERY, int.Parse(startDay.Text), double.Parse(price.Text), contractor.Text, contractee.Text)
                {
                    DestinationPlace = destination.Text,
                    EndDate = int.Parse(endDay.Text),
                    Finished = false,
                    Service = goods.Text,
                    OriginPlace = origin.Text,
                    UpFrontCost = double.Parse(UpFront.Text),
                    UpFrontType = UpfrontType.PAYMENT
                };
                data.Contracts.Add(contract);
                Clear();
            };

            Container.Controls.Add(contractorlbl);
            Container.Controls.Add(contractor);
            Container.Controls.Add(contracteelbl);
            Container.Controls.Add(contractee);
            Container.Controls.Add(goodslbl);
            Container.Controls.Add(goods);
            Container.Controls.Add(startDaylbl);
            Container.Controls.Add(startDay);
            Container.Controls.Add(endDaylbl);
            Container.Controls.Add(endDay);
            Container.Controls.Add(pricelbl);
            Container.Controls.Add(price);
            Container.Controls.Add(UpFrontlbl);
            Container.Controls.Add(UpFront);
            Container.Controls.Add(originlbl);
            Container.Controls.Add(origin);
            Container.Controls.Add(ddestinationlbl);
            Container.Controls.Add(destination);
            Container.Controls.Add(submit);
        }

        private void SingleServiceContractToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clear();

            Label contractorlbl = new Label { Top = 0, Left = 0, Text = "Contractor", Width = 100 };
            TextBox contractor = new TextBox { Top = 0, Left = 100 };
            Label contracteelbl = new Label { Top = 25, Left = 0, Text = "Contractee", Width = 100 };
            TextBox contractee = new TextBox { Top = 25, Left = 100 };
            Label servicelbl = new Label { Top = 50, Left = 0, Text = "Service", Width = 100 };
            TextBox service = new TextBox { Top = 50, Left = 100, Width = 400 };
            Label startDaylbl = new Label { Top = 75, Left = 0, Text = "Start Day", Width = 100 };
            TextBox startDay = new TextBox { Top = 75, Left = 100, Text = "0" };
            Label endDaylbl = new Label { Top = 100, Left = 0, Text = "End Day", Width = 100 };
            TextBox endDay = new TextBox { Top = 100, Left = 100, Text = "0" };
            Label pricelbl = new Label { Top = 125, Left = 0, Text = "Price", Width = 100 };
            TextBox price = new TextBox { Top = 125, Left = 100, Text = "0" };
            Label UpFrontlbl = new Label { Top = 150, Left = 0, Text = "Up Front Cost", Width = 100 };
            TextBox UpFront = new TextBox { Top = 150, Left = 100, Text = "0" };
            Label originlbl = new Label { Top = 175, Left = 0, Text = "Origin Place", Width = 100 };
            TextBox origin = new TextBox { Top = 175, Left = 100 };
            Label ddestinationlbl = new Label { Top = 200, Left = 0, Text = "Destination", Width = 100 };
            TextBox destination = new TextBox { Top = 200, Left = 100 };
            Button submit = new Button { Top = 225, Left = 50, Width = 100, Text = "Agree" };
            submit.Click += (s, ea) => {
                Contract contract = new Contract(ContractType.SINGLE_SERVICE, int.Parse(startDay.Text), double.Parse(price.Text), contractor.Text, contractee.Text)
                {
                    DestinationPlace = destination.Text,
                    EndDate = int.Parse(endDay.Text),
                    Finished = false,
                    Service = service.Text,
                    OriginPlace = origin.Text,
                    UpFrontCost = double.Parse(UpFront.Text),
                    UpFrontType = UpfrontType.PAYMENT
                };
                data.Contracts.Add(contract);
                Clear();
            };

            Container.Controls.Add(contractorlbl);
            Container.Controls.Add(contractor);
            Container.Controls.Add(contracteelbl);
            Container.Controls.Add(contractee);
            Container.Controls.Add(servicelbl);
            Container.Controls.Add(service);
            Container.Controls.Add(startDaylbl);
            Container.Controls.Add(startDay);
            Container.Controls.Add(endDaylbl);
            Container.Controls.Add(endDay);
            Container.Controls.Add(pricelbl);
            Container.Controls.Add(price);
            Container.Controls.Add(UpFrontlbl);
            Container.Controls.Add(UpFront);
            Container.Controls.Add(originlbl);
            Container.Controls.Add(origin);
            Container.Controls.Add(ddestinationlbl);
            Container.Controls.Add(destination);
            Container.Controls.Add(submit);
        }

        private void ListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clear();
            int top = 0;
            foreach(Contract c in data.Contracts)
            {
                Label label = new Label { Top = top, Left = 0, Text = c.ToString(), Width = 550};
                Button cancel = new Button { Top = top, Left = 550, Text = "Cancel Contract", Width = 100 };
                Button detail = new Button { Top = top, Left = 650, Text = "View in detail", Width = 100 };

                cancel.Click += (s, ea) =>
                 {
                     c.Cancel(data.CurrentDate);
                     data.Contracts.Remove(c);
                     ListToolStripMenuItem_Click(s, ea);
                 };

                detail.Click += (s, ea) =>
                  {
                      MessageBox.Show(this, c.FullDetailString(), "Contract detail", MessageBoxButtons.OK, MessageBoxIcon.Information);
                  };

                Container.Controls.Add(label);
                Container.Controls.Add(cancel);
                Container.Controls.Add(detail);

                top += 25;
            }
        }

    }
}
