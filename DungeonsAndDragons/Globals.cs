﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DungeonsAndDragons
{
    [Serializable]
    class Data
    {
        public int CurrentDate = 1;
        public List<Contract> Contracts = new List<Contract>();

        public string NextDay()
        {
            CurrentDate++;
            string message = "It is day " + CurrentDate + "\n";
            foreach(Contract c in Contracts)
            {
                message += c.StatusMessage(CurrentDate);
            }
            return message;
        }
    }
}
