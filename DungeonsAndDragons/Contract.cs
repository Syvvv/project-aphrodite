﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonsAndDragons
{
    [Serializable]
    class Contract
    {
        public ContractType Type { get; set; }
        public int StartDate { get; set; }
        public int EndDate { get; set; }
        public int DayInterval { get; set; }
        public double GoldPrice { get; set; }
        public string Goods { get; set; } = "";
        public string Service { get; set; } = "";
        public double UpFrontCost { get; set; }
        public UpfrontType UpFrontType { get; set; }
        public double Discount { get; set; }
        public int DiscountTime_Remaining { get; set; }
        public string OriginPlace { get; set; } = "";
        public string DestinationPlace { get; set; } = "";
        public string Contracter { get; set; } //the one who pays
        public string Contractee { get; set; } //the one who delivers something for the contract
        public int CancellationPeriod { get; set; }
        public bool Finished { get; set; } //Can the money be cashed in?

        public Contract(ContractType type, int startDate, double goldPrice, string contracter, string contractee)
        {
            Type = type;
            StartDate = startDate;
            GoldPrice = goldPrice;
            Contracter = contracter;
            Contractee = contractee;
        }

        public void Cancel(int currDate)
        {
            EndDate = currDate + CancellationPeriod;
        }

        public string StatusMessage(int currDate)
        {
            if(EndDate == 0)
            {
                EndDate = int.MaxValue;
            }
            string result = "";
            if (Type == ContractType.RECURRING_GOODS)
            {
                if ((currDate - StartDate) % DayInterval == 0)
                {
                    if (DiscountTime_Remaining > 0)
                    {
                        result += Contractee + " delivered " + Goods + " to " + DestinationPlace + " in exchange for " + ToGp(GoldPrice - Discount) + ".\n";
                        DiscountTime_Remaining--;
                    }
                    else
                    {
                        result += Contractee + " delivered " + Goods + " to " + DestinationPlace + " in exchange for " + ToGp(GoldPrice) + ".\n";
                    }
                }
                if (currDate == EndDate)
                {
                    result += Contractee + " will no longer deliver " + Goods + " to " + DestinationPlace + ".\n";
                }
            }
            else if (Type == ContractType.RECURRING_SERVICE)
            {
                if ((currDate - StartDate) % DayInterval == 0)
                {
                    if (DiscountTime_Remaining > 0)
                    {
                        result += Contractee + " will " + Service + " for " + Contracter + " in exchange for " + ToGp(GoldPrice - Discount) + ".\n";
                        DiscountTime_Remaining--;
                    }
                    else
                    {
                        result += Contractee + " will " + Service + " for " + Contracter + " in exchange for " + ToGp(GoldPrice) + ".\n";
                    }
                }
                if (currDate == EndDate)
                {
                    result += Contractee + " will no longer  " + Service + " for " + Contracter + ".\n";
                }
            } else if (Type == ContractType.SINGLE_GOODS_DELIVERY)
            {
                if(!Finished)
                {
                    if(currDate == EndDate)
                    {
                        result += Contractee + " has until today to deliver " + Goods + " to " + DestinationPlace + ".\n";
                    }else if(currDate > EndDate)
                    {
                        result += Contractee + " failed to deliver " + Goods + " to " + DestinationPlace + " on time.\n";
                    }
                }
            }
            else
            {
                if (!Finished)
                {
                    if (currDate == EndDate)
                    {
                        result += Contractee + " has until today to " + Service + " for " + Contracter + ".\n";
                    }
                    else if (currDate > EndDate)
                    {
                        result += Contractee + " failed to " + Service + " for " + Contracter + " on time.\n";
                    }
                }
            }
            return result;
        }

        public string ToGp(double price)
        {
            double gp = Math.Floor(price);
            double sp = Math.Floor((price * 10) % 10);
            double cp = Math.Floor((price * 100) % 10);
            if(gp != 0)
            {
                if (sp != 0)
                {
                    if(cp != 0)
                    {
                        return gp + "gp, " + sp + "sp, " + cp + "cp";
                    }
                    else
                    {
                        return gp + "gp, " + sp + "sp";
                    }
                }
                else{
                    if(cp != 0)
                    {
                        return gp + "gp, " + cp + "cp";
                    }
                    else
                    {
                        return gp + "gp";
                    }
                }
            }else if(sp != 0)
            {
                if(cp != 0)
                {
                    return sp + "sp, " + cp + "cp";
                }
                else
                {
                    return sp + "sp";
                }
            }
            else
            {
                return cp + "cp";
            }
        }

        public override string ToString()
        {
            switch(Type)
            {
                case ContractType.RECURRING_GOODS:
                    if(DiscountTime_Remaining>0)
                    {
                        return Contractee + " will deliver " + Goods + " from " + OriginPlace + " to " + DestinationPlace + " every "+ DayInterval +" days, " + Contracter + " will pay " + ToGp(GoldPrice-Discount);
                    }
                    else
                    {
                        return Contractee + " will deliver " + Goods + " from " + OriginPlace + " to " + DestinationPlace + " every " + DayInterval + " days, " + Contracter + " will pay " + ToGp(GoldPrice);
                    }
                    
                case ContractType.RECURRING_SERVICE:
                    if (DiscountTime_Remaining > 0)
                    {
                        return Contractee + " will " + Service + " every " + DayInterval + " days, " + Contracter + " will pay " + ToGp(GoldPrice - Discount);
                    }
                    else
                    {
                        return Contractee + " will " + Service + " every " + DayInterval + " days, " + Contracter + " will pay " + ToGp(GoldPrice);
                    }
                case ContractType.SINGLE_GOODS_DELIVERY:
                    return Contractee + " will deliver " + Goods + " from " + OriginPlace + " to " + DestinationPlace + ", " + Contracter + " will pay " + ToGp(GoldPrice);
                case ContractType.SINGLE_SERVICE:
                    return Contractee + " will " + Service + ", " + Contracter + " will pay " + ToGp(GoldPrice);
            }
            return "";
        }

        public string FullDetailString()
        {
            string result = "";

            if (Type == ContractType.RECURRING_GOODS || Type == ContractType.SINGLE_GOODS_DELIVERY)
            {
                result += Contractee + " will deliver " + Goods + " from " + OriginPlace + " to " + DestinationPlace + " for " + Contracter + ".\n";
                if (Type == ContractType.RECURRING_GOODS)
                {
                    result += "This delivery will occur every " + DayInterval + " days starting on day " + StartDate + ".\n";
                }
                else if (EndDate != int.MaxValue && EndDate != 0)
                {
                    result += "This delivery will have to be delivered in whole by day " + EndDate + ".\n";
                }
            }
            else
            {
                result += Contractee + " will " + Service + " for " + Contracter + ".\n";
                if (Type == ContractType.RECURRING_SERVICE)
                {
                    result += "This service will occur every " + DayInterval + " days starting on day " + StartDate + ".\n";
                }
                else if (EndDate != int.MaxValue && EndDate != 0)
                {
                    result += "This service will have to be finished by day " + EndDate + ".\n";
                }
            }
            if (DiscountTime_Remaining > 0)
            {
                result += "The next " + DiscountTime_Remaining + " times the price will be " + ToGp(GoldPrice - Discount) + ".\n";
            }
            result += "The regular price is " + ToGp(GoldPrice);
            if (Type == ContractType.RECURRING_GOODS||Type == ContractType.RECURRING_SERVICE)
            {
                result += " every time";
            }
            result += ".\n";
            if(CancellationPeriod>0)
            {
                result += "There is a cancellation period in place of " + CancellationPeriod + "days.\n";
            }
            if(Finished)
            {
                result += "This contract is already done!";
            }
            return result;
        }

    }

    enum ContractType
    {
        RECURRING_GOODS, RECURRING_SERVICE, SINGLE_GOODS_DELIVERY, SINGLE_SERVICE
    }

    enum UpfrontType
    {
        DEPOSIT, PAYMENT
    }
}
